package edu.horyzont.repository;

import edu.horyzont.domain.City;
import edu.horyzont.domain.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PaymentRepository extends JpaRepository<Payment,Long> {
    @Query("SELECT p FROM Payment p left JOIN FETCH p.customer")
    Page<Payment> findAllWithCustomers(Pageable pageable);
}
