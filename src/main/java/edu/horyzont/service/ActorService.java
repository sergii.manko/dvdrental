package edu.horyzont.service;

import edu.horyzont.domain.Actor;
import edu.horyzont.repository.ActorRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ActorService {

    private final ActorRepository actorRepository;

    public void add(String firstName,
                    String lastName) {
        var actor = new Actor(firstName,
                lastName);
        actorRepository.save(actor);
    }

    public Actor findById(Long actorId) {
        return actorRepository.findById(actorId)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    public void update(Long actorId,
                       String actorFirstName,
                       String actorLastName) {
        actorRepository.findById(actorId)
                .ifPresent(a -> {
                    a.setFirstName((actorFirstName));
                    a.setLastName((actorLastName));
                });
    }

    public Page<Actor> findAll(final Pageable pageable) {
        return actorRepository.findAll(pageable);
    }

    public List<Actor> findAll() {
        return actorRepository.findAll();
    }

    public void delete(final Long actorId) {
        actorRepository.deleteById(actorId);
    }

    public List<Actor> findAllById(Set<Long> actorsIds) {
        return actorRepository.findAllById(actorsIds);
    }

}
