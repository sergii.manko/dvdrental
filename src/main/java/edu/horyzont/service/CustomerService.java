package edu.horyzont.service;

import edu.horyzont.domain.Customer;
import edu.horyzont.domain.dto.CustomerDto;
import edu.horyzont.domain.dto.CustomerPaymentDto;
import edu.horyzont.domain.dto.CustomerView;
import edu.horyzont.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final AddressService addressService;

    public void add(final String firstName,
                    final String lastName,
                    final String email,
                    final Long addressId) {
        Customer customer = new Customer(firstName,
                                         lastName,
                                         email);
        customer.setAddress(addressService.findById(addressId));
        customerRepository.save(customer);
    }

    @Transactional
    public void update(final Long customerId,
                       final String firstName,
                       final String lastName,
                       final String email) {
        customerRepository.findById(customerId)
                          .ifPresent(c -> {
                              c.setFirstName(firstName);
                              c.setLastName(lastName);
                              c.setEmail(email);
                          });

    }

    @Transactional(readOnly = true)
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Transactional(readOnly = true)
    public List<CustomerPaymentDto> getAll() {
        return customerRepository.findAll().stream().map(this::mapToCustomerPaymentDto).toList();
    }

    private CustomerPaymentDto mapToCustomerPaymentDto(Customer customer) {
        return new CustomerPaymentDto(customer.getCustomerId(),
                String.format("%s %s", customer.getFirstName(), customer.getLastName()));
    }

    public CustomerView getCustomersForView(Pageable pageable) {
        final Page<CustomerDto> customerDtoPage = customerRepository.getCustomersForView(pageable);
        var customerView = new CustomerView();
        customerView.setCustomers(customerDtoPage.getContent());
        customerView.setNumber(customerDtoPage.getNumber());
        customerView.setTotalElements(customerDtoPage.getTotalElements());
        customerView.setTotalPages(customerDtoPage.getTotalPages());
        return customerView;
    }

    @Transactional
    public Page<Customer> findAll(final Pageable pageable) {
        return customerRepository.findAll(pageable);
    }

    public void delete(final Long id) {
        customerRepository.deleteById(id);
    }

}
