package edu.horyzont.controller.payment;

import edu.horyzont.controller.BaseController;
import edu.horyzont.service.CustomerService;
import edu.horyzont.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/payment")
@RequiredArgsConstructor
public class PaymentListController extends BaseController {
    private final PaymentService paymentService;
    private final CustomerService customerService;

    @GetMapping("/list")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ROLE_MANAGER')")
    public String list( final Model model,
                               @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                               @PageableDefault(sort = "paymentId") Pageable pageable) {
        addUsernameToModel(model);
        model.addAttribute("payments", paymentService.findAll(pageable));
        model.addAttribute("customers", customerService.getAll());

        return "payment/list";
    }
}
