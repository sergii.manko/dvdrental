package edu.horyzont.controller.payment;

import edu.horyzont.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
@RequestMapping("payment")
public class PaymentAddController {
    private final PaymentService paymentService;
    @PostMapping("/add")
    public String add( @RequestParam("paymentAmount") final Double amount,
                       @RequestParam("customerId") final Long customerId){
        paymentService.add(amount, customerId);
        return "redirect:/payment/list";
    }
}
