package edu.horyzont.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class PaymentDto {
    private Long paymentId;
    private Double amount;
    private LocalDateTime paymentDate;
    private LocalDateTime lastUpdate;
    private String customerName;
}
