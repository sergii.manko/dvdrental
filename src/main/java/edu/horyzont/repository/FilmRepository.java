package edu.horyzont.repository;

import edu.horyzont.domain.Film;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface FilmRepository extends JpaRepository<Film, Long> {

    String QUERY = """
                 SELECT\s
                     f.film_id as filmId,
                     f.title,
                     f.description,
                     f.release_year as releaseYear,
                     f.rental_duration as rentalDuration,
                     f.rental_rate as rentalRate,
                     f.film_length as filmLength,
                     f.replacement_cost as replacementCost,
                     STRING_AGG(DISTINCT c.name, ', ') as cn,
                     STRING_AGG(DISTINCT concat(a.first_name, ' ', a.last_name), ', ') as fa,
                     ARRAY_AGG(DISTINCT c.category_id) as categoryIds,
                     ARRAY_AGG(DISTINCT a.actor_id) as actorIds
                 FROM film f
                 LEFT JOIN film_category fc ON f.film_id = fc.film_id
                 LEFT JOIN category c ON fc.category_id = c.category_id
                 LEFT JOIN film_actor fa ON f.film_id = fa.film_id
                 LEFT JOIN actor a ON fa.actor_id = a.actor_id
                 GROUP BY f.film_id
            \s""";


    @Query(value = QUERY, nativeQuery = true)
    Page<Object[]> getFilmsForView(Pageable pageable);

}





