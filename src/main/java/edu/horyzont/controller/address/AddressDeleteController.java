package edu.horyzont.controller.address;

import edu.horyzont.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/address")
public class AddressDeleteController {
    private final AddressService addressService;

    @PostMapping("/delete")
    public String delete(final Long addressId) {
        addressService.delete(addressId);
        return "redirect:/address/list";
    }
}
