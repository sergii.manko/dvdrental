package edu.horyzont.repository;

import edu.horyzont.domain.Customer;
import edu.horyzont.domain.dto.CustomerDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerRepository
        extends JpaRepository<Customer, Long> {

    @Query("SELECT new edu.horyzont.domain.dto.CustomerDto(c.customerId, c.firstName, c.lastName, c.email, c.active, a.city.cityTitle, a.postalCode, a.phone, c.lastUpdate) " +
           "FROM Customer c JOIN  c.address a JOIN  a.city")
    Page<CustomerDto> getCustomersForView(Pageable pageable);

}
