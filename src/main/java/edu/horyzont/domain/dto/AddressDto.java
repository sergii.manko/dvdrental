package edu.horyzont.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class AddressDto {
    private Long addressId;
    private String address;
    private String address2;
    private String district;
    private String cityTitle;
    private String postalCode;
    private String phone;
    private LocalDateTime lastUpdate;
}
