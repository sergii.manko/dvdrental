package edu.horyzont.service;

import edu.horyzont.domain.Address;
import edu.horyzont.domain.dto.AddressDto;
import edu.horyzont.repository.AddressRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressService {
    private final AddressRepository addressRepository;
    private final CityService cityService;


    public void add(String address, String address2, String district, Long cityId,
                    String postalCode, String phone) {
        var addressObj = new Address(address, address2, district, postalCode, phone);
        addressObj.setCity(cityService.findById(cityId));
        addressRepository.save(addressObj);
    }

    @Transactional
    public void update(Long addressId, String address, String address2, String district, Long cityId,
                       String postalCode, String phone) {
        addressRepository.findById(addressId).ifPresent(a -> {
            a.setAddress(address);
            a.setAddress2(address2);
            a.setDistrict(district);
            a.setCity(cityService.findById(cityId));
            a.setPostalCode(postalCode);
            a.setPhone(phone);
        });
    }

    public void delete(Long addressId) {
        addressRepository.deleteById(addressId);
    }

    @Transactional
    public List<Address> findAll() {
        return addressRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Page<AddressDto> findAll(final Pageable pageable) {
        Page<Address> all = addressRepository.findAllWithCities(pageable);
        List<AddressDto> list = all.get().map(this::mapToDto).toList();
        return new PageImpl<>(list, pageable, all.getTotalElements());
    }

    private AddressDto mapToDto(Address address) {
        return AddressDto.builder()
                .cityTitle(address.getCity().getCityTitle())
                .lastUpdate(address.getLastUpdate())
                .addressId(address.getAddressId())
                .address(address.getAddress())
                .address2(address.getAddress2())
                .phone(address.getPhone())
                .district(address.getDistrict())
                .postalCode(address.getPostalCode())
                .build();
    }

    @Transactional(readOnly = true)
    public Address findById(Long addressId) {
        return addressRepository.findById(addressId).orElseThrow(EntityNotFoundException::new);
    }
}
