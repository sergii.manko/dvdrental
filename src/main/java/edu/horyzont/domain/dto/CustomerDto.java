package edu.horyzont.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Serhii Manko
 */
@Data
@AllArgsConstructor
public class CustomerDto {

    private Long customerId;
    private String firstName;
    private String lastName;
    private String email;
    private boolean active;
    private String cityTitle;
    private String postalCode;
    private String phone;
    private LocalDateTime lastUpdate;

}
