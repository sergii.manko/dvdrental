package edu.horyzont.domain.dto;

import lombok.Data;

/**
 * @author Serhii Manko
 */
@Data
public abstract class PaginationDto {

    protected int totalPages;
    protected long totalElements;
    protected int number;

}
