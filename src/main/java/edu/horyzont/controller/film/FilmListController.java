package edu.horyzont.controller.film;

import edu.horyzont.controller.BaseController;
import edu.horyzont.service.ActorService;
import edu.horyzont.service.CategoryService;
import edu.horyzont.service.FilmService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/film/list")
@RequiredArgsConstructor
public class FilmListController extends BaseController {

  private final FilmService filmService;
  private final CategoryService categoryService;
  private final ActorService actorService;

  @GetMapping
  public String list(
      final Model model,
      @RequestParam(value = "page", required = false, defaultValue = "0") int page,
      @PageableDefault( sort = "filmId") Pageable pageable) {
    addUsernameToModel(model);
    model.addAttribute("films", filmService.getFilmsForView(pageable));
    model.addAttribute("categories", categoryService.findAll());
    model.addAttribute("actors", actorService.findAll());
    return "/film/list";
  }

}
