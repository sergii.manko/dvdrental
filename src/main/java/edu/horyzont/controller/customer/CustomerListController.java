package edu.horyzont.controller.customer;

import edu.horyzont.controller.BaseController;
import edu.horyzont.service.AddressService;
import edu.horyzont.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
@RequestMapping("/customers")
public class CustomerListController extends BaseController {

    private final CustomerService customerService;
    private final AddressService addressService;

    @GetMapping("/list")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    public String list(Model model,
                       @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                       @PageableDefault(sort = "customerId") Pageable pageable) {
        addUsernameToModel(model);
        model.addAttribute("customerView",
                           customerService.getCustomersForView(pageable));
        model.addAttribute("addresses",
                           addressService.findAll());
        return "customer/list";

    }

}
