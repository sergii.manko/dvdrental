package edu.horyzont.service;

import edu.horyzont.domain.Customer;
import edu.horyzont.domain.Payment;
import edu.horyzont.domain.dto.PaymentDto;
import edu.horyzont.repository.CustomerRepository;
import edu.horyzont.repository.PaymentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor

public class PaymentService {
    private final PaymentRepository paymentRepository;
    private final CustomerRepository customerRepository;

    public List<Payment> findAll(){
        return paymentRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Page<PaymentDto> findAll(final Pageable pageable) {
        Page<Payment> all = paymentRepository.findAllWithCustomers(pageable);
        List<PaymentDto> list = all.get().map(this::mapToDto).toList();
        return new PageImpl<>(list, pageable, all.getTotalElements());
    }

    private PaymentDto mapToDto(Payment payment){
        return PaymentDto.builder()
                .paymentId(payment.getPaymentId())
                .amount(payment.getAmount())
                .paymentDate(payment.getPaymentDate())
                .lastUpdate(payment.getLastUpdate())
                .customerName(String.format("%s %s", payment.getCustomer().getFirstName(), payment.getCustomer().getLastName()))
                .build();
    }

    public void delete(final Long paymentId){
        paymentRepository.deleteById(paymentId);
    }

    @Transactional
    public Payment add(final Double amount, Long customerId){
        Payment payment = new Payment(amount);
        Customer customer = customerRepository.findById(customerId).orElseThrow();
        payment.setCustomer(customer);
        return paymentRepository.save(payment);
    }

    public void update(final Long paymentId, final Double amount){
        paymentRepository.findById(paymentId).ifPresent(p -> p.setAmount(amount));
    }
}
