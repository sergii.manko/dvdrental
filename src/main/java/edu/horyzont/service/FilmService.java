package edu.horyzont.service;

import edu.horyzont.domain.Film;
import edu.horyzont.domain.dto.FilmDto;
import edu.horyzont.repository.FilmRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class FilmService {

    private final FilmRepository filmRepository;
    private final CategoryService categoryService;
    private final ActorService actorService;

    public Film findById(final Long id) {
        return filmRepository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
    }

    public Page<FilmDto> getFilmsForView(Pageable pageable) {
        Page<Object[]> result = filmRepository.getFilmsForView(pageable);

        return result.map(this::mapToFilmDto);
    }

    private FilmDto mapToFilmDto(Object[] tuple) {
        return new FilmDto(
                (Long) tuple[0], // filmId
                (String) tuple[1], // title
                (String) tuple[2], // description
                (String) tuple[3], // releaseYear
                (Integer) tuple[4], // rentalDuration
                (Double) tuple[5], // rentalRate
                (Integer) tuple[6], // filmLength
                (Double) tuple[7], // replacementCost
                tuple[8], // categories (can be cast to appropriate type)
                tuple[9], // actors (can be cast to appropriate type)
                Set.copyOf(List.of((Long[]) tuple[10])), // categoryIds (cast to Set<Long>)
                Set.copyOf(List.of((Long[]) tuple[11]))  // actorIds (cast to Set<Long>)
        );
    }

    public void delete(final Long filmId) {
        filmRepository.deleteById(filmId);
    }

    public void update(final FilmDto dto) {
        filmRepository.findById(dto.getFilmId())
                .ifPresent(film -> {
                    fillFilmEntity(dto, film);
                    var categories = categoryService.findAllById(dto.getCategoryIds());
                    film.addCategories(categories);
                    var actors = actorService.findAllById(dto.getActorIds());
                    film.getActors()
                            .clear();
                    film.getActors()
                            .addAll(actors);
                    filmRepository.save(film);
                });
    }

    public static void fillFilmEntity(FilmDto dto, Film film) {
        film.setTitle(dto.getTitle());
        film.setDescription(dto.getDescription());
        film.setReleaseYear(dto.getReleaseYear());
        film.setRentalDuration(dto.getRentalDuration());
        film.setRentalRate(dto.getRentalRate());
        film.setFilmLength(dto.getFilmLength());
        film.setReplacementCost(dto.getReplacementCost());
    }

    public void add(final FilmDto dto) {
        var film = Film.of(dto);
        var categories = categoryService.findAllById(dto.getCategoryIds());
        var actors = actorService.findAllById(dto.getActorIds());
        film.getCategories()
                .addAll(categories);
        film.getActors()
                .addAll(actors);
        filmRepository.save(film);

    }

}
