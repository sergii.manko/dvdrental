package edu.horyzont.repository;

import edu.horyzont.domain.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CityRepository extends JpaRepository<City,Long> {
    @Query("SELECT c FROM City c left JOIN FETCH c.country")
    Page<City> findAllWithCountries(Pageable pageable);
}
