ALTER TABLE Payment
    ADD COLUMN customer_id BIGINT NOT NULL ;

ALTER TABLE Payment
    ADD CONSTRAINT fk_customer
        FOREIGN KEY (customer_id) REFERENCES Customer(customer_id);