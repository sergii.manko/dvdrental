package edu.horyzont.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerPaymentDto {
    private Long customerId;
    private String name;
}
