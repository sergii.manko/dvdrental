package edu.horyzont;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DvdRentalApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(DvdRentalApplication.class, args);
	}
}
