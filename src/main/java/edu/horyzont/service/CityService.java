package edu.horyzont.service;

import edu.horyzont.domain.City;
import edu.horyzont.domain.dto.CityDto;
import edu.horyzont.repository.CityRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CityService {
    private final CityRepository cityRepository;
    private final CountryService countryService;

    public void add(final String cityTitle, final Long countryId) {
        City city = new City(cityTitle);
        city.setCountry(countryService.findById(countryId));
        cityRepository.save(city);
    }
    @Transactional
    public void update(final Long cityId, final String cityTitle, final Long countryId) {
        cityRepository.findById(cityId).ifPresent(city -> {
                    city.setCityTitle(cityTitle);
                    city.setCountry(countryService.findById(countryId));
                }
        );
    }

    public City findById(final Long cityId){
        return cityRepository.findById(cityId).orElseThrow(EntityNotFoundException::new);    }

    public void delete(final Long cityId) {
        cityRepository.deleteById(cityId);
    }

    public List<City> findAll() {
        return cityRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Page<CityDto> findAll(final Pageable pageable) {
        Page<City> all = cityRepository.findAllWithCountries(pageable);
        List<CityDto> list = all.get().map(this::mapToDto).toList();
        return new PageImpl<>(list, pageable, all.getTotalElements());
    }

    private CityDto mapToDto(City city){
        return CityDto.builder()
                .cityId(city.getCityId())
                .cityTitle(city.getCityTitle())
                .lastUpdate(city.getLastUpdate())
                .countryTitle(city.getCountry().getCountryTitle())
                .build();
    }

}
