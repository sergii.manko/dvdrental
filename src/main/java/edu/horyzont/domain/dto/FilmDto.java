package edu.horyzont.domain.dto;

import edu.horyzont.domain.Actor;
import edu.horyzont.domain.Category;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
public class FilmDto {

  private Long filmId;
  private String title;
  private String description;
  private String releaseYear;
  private Integer rentalDuration;
  private Double rentalRate;
  private Integer filmLength;
  private Double replacementCost;

  public FilmDto(Long filmId,
                 String title,
                 String description,
                 String releaseYear,
                 Integer rentalDuration,
                 Double rentalRate,
                 Integer filmLength,
                 Double replacementCost,
                 Object categories,
                 Object actors,
                 Set<Long> categoryIds,
                 Set<Long> actorIds) {
    this.filmId = filmId;
    this.title = title;
    this.description = description;
    this.releaseYear = releaseYear;
    this.rentalDuration = rentalDuration;
    this.rentalRate = rentalRate;
    this.filmLength = filmLength;
    this.replacementCost = replacementCost;
    this.categories = categories;
    this.actors = actors;
    this.categoryIds = categoryIds;
    this.actorIds = actorIds;
  }

  private Object categories;
  private Object actors;
  private Set<Long> categoryIds;
  private Set<Long> actorIds;
  private LocalDateTime lastUpdate;
  private List<Category> categoryList;
  private List<Actor> actorList;

  

}
