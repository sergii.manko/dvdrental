package edu.horyzont.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class CityDto {
    private Long cityId;

    private String cityTitle;

    private LocalDateTime lastUpdate;

    private String countryTitle;
}
