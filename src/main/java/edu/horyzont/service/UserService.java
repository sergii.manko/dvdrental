package edu.horyzont.service;

import edu.horyzont.domain.Role;
import edu.horyzont.domain.User;
import edu.horyzont.domain.dto.AuthorizationToken;
import edu.horyzont.domain.dto.JwtAuthorizationResponse;
import edu.horyzont.exception.AuthenticationException;
import edu.horyzont.exception.UserExistsException;
import edu.horyzont.repository.RoleRepository;
import edu.horyzont.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;


@Service
@RequiredArgsConstructor
@Transactional
public class UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;


    private static final String USER_EXISTS_ERROR_MESSAGE = "User with email %s already exists";


    public void saveRegisteredUser(String username, String password) {
        if (userRepository.existsByEmail(username)) {
            throw new UserExistsException(String.format(USER_EXISTS_ERROR_MESSAGE, username));
        }
        User user = new User();
        user.setEmail(username);
        user.setPassword(passwordEncoder.encode(password));
        Role roleUser = roleRepository.findRoleByName("ROLE_USER");
        user.setRole(roleUser);
        userRepository.save(user);
    }

    public JwtAuthorizationResponse authorize(String username, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (BadCredentialsException e) {
            throw new AuthenticationException("Email or password is wrong, try again");
        }
        User user = userRepository.findUserByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with email " + username + " doesn`t exist"));
        String token = jwtService.generateToken(user);
        OffsetDateTime expiresAt = jwtService.getExpiration(token);
        List<String> permissions = user.getAuthorities().stream().map(GrantedAuthority::getAuthority).toList();
        return new JwtAuthorizationResponse(new AuthorizationToken(token, expiresAt), user.getRole().getName(), permissions);
    }
}
