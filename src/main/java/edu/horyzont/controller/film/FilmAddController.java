package edu.horyzont.controller.film;

import edu.horyzont.domain.dto.FilmDto;
import edu.horyzont.service.FilmService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/film")
@RequiredArgsConstructor
public class FilmAddController {

  private final FilmService filmService;

  @PostMapping("/add")
  public String add(final FilmDto dto) {
    filmService.add(dto);
    return "redirect:/film/list";
  }

}
