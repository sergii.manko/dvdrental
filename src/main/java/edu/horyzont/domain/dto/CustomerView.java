package edu.horyzont.domain.dto;

import lombok.Data;

import java.util.List;

/**
 * @author Serhii Manko
 */
@Data
public class CustomerView
        extends PaginationDto {

    private List<CustomerDto> customers;

}
