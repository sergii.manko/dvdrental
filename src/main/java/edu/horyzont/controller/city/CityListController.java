package edu.horyzont.controller.city;

import edu.horyzont.controller.BaseController;
import edu.horyzont.service.CityService;
import edu.horyzont.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/city")
@RequiredArgsConstructor
public class CityListController extends BaseController {
    private final CityService cityService;
    private final CountryService countryService;

    @GetMapping("/list")
    public String list(Model model,
                           @RequestParam(value = "page", required = false, defaultValue = "0") int page,
                           @PageableDefault(value = 10, page = 0, sort = "cityId") Pageable pageable) {
        addUsernameToModel(model);
        model.addAttribute("cities", cityService.findAll(pageable));
        model.addAttribute("countries", countryService.findAll());
        return "city/list";
    }
}
