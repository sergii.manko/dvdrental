package edu.horyzont.repository;

import edu.horyzont.domain.Address;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AddressRepository extends JpaRepository<Address, Long> {
    @Query("SELECT a FROM Address a left JOIN FETCH a.city")
    Page<Address> findAllWithCities(Pageable pageable);
}
