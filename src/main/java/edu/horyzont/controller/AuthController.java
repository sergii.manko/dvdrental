package edu.horyzont.controller;

import edu.horyzont.domain.dto.JwtAuthorizationResponse;
import edu.horyzont.domain.dto.UserSavingDto;
import edu.horyzont.service.UserService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequiredArgsConstructor
@RequestMapping()
@Slf4j
public class AuthController {
    private final UserService userService;

    @GetMapping("/login")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView("login");
        modelAndView.addObject("loginDto", new UserSavingDto());
        return modelAndView;
    }

    @PostMapping("/auth/login")
    public ModelAndView login(@ModelAttribute @Valid UserSavingDto loginDto, Errors errors, HttpServletResponse response) {
        if (errors.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("login");
            modelAndView.addObject("loginDto", new UserSavingDto());
            return modelAndView;
        }
        JwtAuthorizationResponse authorize = userService.authorize(loginDto.getUsername(), loginDto.getPassword());
        Cookie jwtCookie = new Cookie("jwtToken", authorize.getAuthorizationToken().getToken());
        jwtCookie.setHttpOnly(true);
        jwtCookie.setPath("/");
        response.addCookie(jwtCookie);

        ModelAndView modelAndView = new ModelAndView("redirect:/");
        log.info("User {} logged in", loginDto.getUsername());
        return modelAndView;
    }

    @GetMapping("/register")
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView("register");
        modelAndView.addObject("userDto", new UserSavingDto());
        return modelAndView;
    }

    @PostMapping("auth/register")
    public ModelAndView register(@ModelAttribute @Valid UserSavingDto userDto, Errors errors) {
        if (errors.hasErrors()) {
            ModelAndView modelAndView = new ModelAndView("register");
            modelAndView.addObject("userDto", new UserSavingDto());
            return modelAndView;
        }
        userService.saveRegisteredUser(userDto.getUsername(), userDto.getPassword());
        return new ModelAndView("redirect:/auth/login");
    }

    @GetMapping("/auth/logout")
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
        SecurityContextHolder.clearContext();

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("jwtToken".equals(cookie.getName())) {
                    cookie.setValue(null);
                    cookie.setPath("/");
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                }
            }
        }

        return new ModelAndView("redirect:/login");
    }
}
