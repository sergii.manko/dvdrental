insert into role(name)
values ('ROLE_ADMIN');

insert into role(name)
values ('ROLE_MANAGER');

insert into role(name)
values ('ROLE_USER');
-- password for admin and manager is 1234567890
insert into users(email, password, role_id)
values ('admin@gmail.com', '$2a$12$NyXJqFMmSQbCoeaJ7iRQvuB.yZ30nbb7gLaNTzV2V1N/Pc3jR2Lwa', 1);
insert into users(email, password, role_id)
values ('manager@gmail.com', '$2a$12$NyXJqFMmSQbCoeaJ7iRQvuB.yZ30nbb7gLaNTzV2V1N/Pc3jR2Lwa', 2);