package edu.horyzont.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long countryId;
    private String countryTitle;
    private LocalDateTime lastDate = LocalDateTime.now();
    @Setter(AccessLevel.PRIVATE)
    @OneToMany(mappedBy = "country")
    private List<City> cities = new ArrayList<>();
    public Country(String countryTitle) {
        this.countryTitle = countryTitle;
    }
    public void addCity(City city) {
        cities.add(city);
        city.setCountry(this);
    }
}
