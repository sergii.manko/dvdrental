package edu.horyzont.domain.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author Serhii Manko
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class PageView<T>
        extends PaginationDto {

    private List<T> content;

}
